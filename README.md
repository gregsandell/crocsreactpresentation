# The Lessons

1. **lesson\_01\_empty.html**:  A starter file.  Our whole React app will be inside the **&lt;script&gt;** block.  The entire output will be placed in the **&lt;div&gt;**.

1. **lesson\_02\_Hello.html**: Our first React app.  We are making a component called **Hello**.  

    a. Note lines 13 and 18 are HTML, mixed into the Javascript.  This syntax is called **JSX**, which stands for *JavaScript eXtension*.
    
    b. How does the browser understand JSX?  It uses the *Babel* transpiler.  You can see references to Babel in lines 5 and 10.  We will talk about Babel later.
    
1. **lesson\_03\_addProp.html**: We have given **&lt;Hello&gt;** a parameter so we can pass in a custom name.  We use an HTML *property* to pass in the parameter.  The component receives the property through **this.props**.  **&lt;Hello&gt;** is now a *reusable component*.

1. **&lt;Hello&gt;** gets its first *state* parameter.  You use state to represent the *data model* of the component.  

1. **lesson\_04\_addState.html**: We set aside properties for a moment to show a different way of supplying data to our component:  through **state**.   

1. **lesson\_05\_addButton.html**: This is our first instance demonstrating the React principle of *"The UI as a function of its state"*.  We have a button that changes **&lt;Hello&gt;**'s name using **updateMessage()**.  It does it only by acting on state; updateMessage()it has no awareness of the component's HTML.  A few facts about state:

    a. You can only modify state with the **this.getState()** method.  You pass getState() an object consisting of the modified properties.  React does not let you say:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*this.state.firstname = Jodi*
    
    b.  State and the *render()* method have a special relationship.  render() is called every time any property in state changes.  This is how state drives the UI.
    
1. **lesson\_06\_dynamicButton.html**: 

